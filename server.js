const express = require('express');
const app = express();
const port = 8877;

const logger = (req, res, next) => {
  console.log('req:', req.url, '\nmethod: ', req.method, '\nbody: ', req.body);
  next();
};

app.use(logger);
app.use(express.json())
app.use(express.static('public'))

app.get('/health', (req, res) => {
  res.send('Yes!! Running');
});

app.post('/payloads-receivable', (req, res) => {
    console.log('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
    console.log(req.body);
    console.log('-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-');
    res.send('{"status": "OK", "nextPing": 15}');
});


app.listen(port, () => console.log(`app listening on port ${port}!`));

