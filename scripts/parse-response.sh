#!/bin/bash

outputFilename=output.json
rawFiles=`ls sample*`

echo $rawFiles
echo '[' > $outputFilename

for file in $rawFiles; do
  echo "Processing file $file"
  echo "{\"request_to\": \"`cat $file | head -n1`\"," >> $outputFilename
  echo "\"timestamp\": \"`ls $file | stat -f "%Sm" -t "%Y-%m-%d"`\"," >> $outputFilename
  echo "\"payload\": `sed '1,/^\s*$/d' $file`}," >> $outputFilename
#  echo "\"payload\": `sed -n -e '/\n/,$p' $file`}," >> $outputFilename
done

echo '{}]' >> $outputFilename

#cat result.*