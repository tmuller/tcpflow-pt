#!/bin/bash

# 1. Start data collection
#tcpdump -s 0 -i eth0 -n -U -w - port 8877
sudo tcpdump -s 0 -i all -n -U -w local-trial.out \
     -c 5 port 8877 | tshark -T json > sample.json

# 1b. -> reduce data to lower bandwidth


# 2. send data to Data Governor
curl --request POST -L \
     -d local-trial.out \
     --url 'http://localhost:8877/gimme-data'\
     --header "Content-Type:raw" \
     --output './local-trial.curl'

