FROM node:12 as frontendBuilder

ENV NODEJS_VERSION="12.19.0"

#SHELL ["/bin/bash", "-o", "pipefail", "-c"]

RUN mkdir -p application
WORKDIR application

RUN apt-get update
RUN apt-get install -y tcpflow

COPY package.json .
RUN npm install

COPY server.js .
COPY scripts/run-monitoring.sh .

CMD ./run-monitoring.sh

EXPOSE 8877
