const DataUpdater = {

  MAX_COLS: 3,
  MAX_ROWS_PER_COLUMN: 5,
  POLLING_INTERVAL: 1000,

  liItem: document.createElement('li'),

  init: () => {
    DataUpdater.liItem = document.getElementById('api-template');
    DataUpdater.pollService();
    const urlParams = DataUpdater.parseQueryString();
    const pollingIntervalMs = urlParams.interval || DataUpdater.POLLING_INTERVAL;
    setInterval(DataUpdater.pollService, pollingIntervalMs);
  },

  parseQueryString: () => {
    const queryString = window.location.search;
    const urlParameters = {};
    queryString.replace(/\??([^=]+)=([^&]+)&?/g, (a,b,c) => {
      urlParameters[b] = c;
    });
    return urlParameters;
  },

  pollService: () => {
    var oReq = new XMLHttpRequest();
    oReq.addEventListener("load", DataUpdater.processServerResponse);
    oReq.open("GET", "http://localhost:8877/example-api-data.json");
    oReq.send();
  },

  processServerResponse: rawData => {
    const apiData = DataUpdater.parseAggregratePayload(rawData);
    if (Object.keys(apiData).length > 0) DataUpdater.renderHtml(apiData);
    else DataUpdater.renderNoContent()
  },

  parseAggregratePayload: jsonPayLoad => JSON.parse(jsonPayLoad.target.response + ']')
      .map(x => x._source.layers)
      .reduce(DataUpdater.aggregatePayload, {}),

  aggregatePayload:  (acc, item) => {
    if (! item['http.response_for.uri']) return acc;
    const service = DataUpdater.extractServiceUrl(item['http.response_for.uri'][0]);
    acc[service] = item;
    return acc;
  },

  extractServiceUrl: payload => payload.split('/')
      .slice(3)
      .join('/')
      .replace(/\b[0-9a-f]{8}-([0-9a-f]{4}-){3}[0-9a-f]{12}/, ':uuid'),

  renderNoContent: () => {
    const p = document.createElement('p');
    p.setAttribute('id', 'all-api-container');
    p.setAttribute('class', 'nocontent-style');
    p.appendChild(document.createTextNode("No content ..."));

    document.getElementById('all-api-container').replaceWith(p);
  },

  renderHtml: apiUrlsWithKeys => {
    const container = document.createElement('ul');
    container.setAttribute('class', 'apilist-container');
    container.setAttribute('id', 'all-api-container');

    const liItem = DataUpdater.liItem.cloneNode(true);
    const renderFct = DataUpdater.renderOneApiContainer(liItem)
    for (const [url, apiFields] of Object.entries(apiUrlsWithKeys)) {
      if (! apiFields['json.key'] || url.indexOf('locales') > -1  ) continue;
      apiFields['json.key'].sort();
      const processed = DataUpdater.deduplicate(apiFields['json.key']);
      const renderedFieldList = renderFct(url, processed);
      container.appendChild(renderedFieldList);
    }

    document.getElementById('all-api-container').replaceWith(container);
  },

  renderOneApiContainer: liElement => (url, apiFieldList) => {
    const apiEndpointNode = document.createElement('li');
    apiEndpointNode.setAttribute('class', 'api-endpoint-container clearfix');

    // Generate the headline
    const headlineNode = document.createElement('h3');
    headlineNode.appendChild(document.createTextNode(url));
    apiEndpointNode.appendChild(headlineNode);

    // Generate the colored type indicator before headline
    const firstSegment = url.split('/').shift();
    let typeClass = '';
    switch (firstSegment) {
      case 'api':
        typeClass = 'api-private';
        break;

      case 'monitor':
        typeClass = 'monitor';
        break;

      default:
        typeClass = 'api-public';
        break;
    }

    // const typeClass = ( == 'api') ? 'api-private' : 'api-public';
    const currentClasses = apiEndpointNode.getAttribute('class').split(' ');
    currentClasses.push(typeClass);
    apiEndpointNode.setAttribute('class', currentClasses.join(' '));

    const columnedData = DataUpdater.createColumns(apiFieldList);
    const ul = document.createElement('ul');
    columnedData.forEach(DataUpdater.renderOneKeyColumn(ul));

    apiEndpointNode.appendChild(ul);
    return apiEndpointNode;
  },

  createColumns: objectPropertyNames => {
    const maxCols = DataUpdater.MAX_COLS;
    const maxRowsPerCol = DataUpdater.MAX_ROWS_PER_COLUMN;
    let columns = [];
    const naturalColumnCount = Math.ceil(objectPropertyNames.length / maxRowsPerCol);
    const columnCount = (naturalColumnCount > maxCols) ? maxCols : naturalColumnCount;
    const itemsPerColumn = Math.ceil(objectPropertyNames.length / columnCount);

    for (let idx = 0; idx < columnCount; idx++) {
      columns.push(objectPropertyNames.slice(idx * itemsPerColumn, (idx + 1) * itemsPerColumn));
    }

    return columns;
  },

  renderOneKeyColumn: ulRoot => columnData => {
    const columnRootLiTag = document.createElement('li');
    columnRootLiTag.setAttribute('class', 'key-column');
    const tagListRootNode = document.createElement('ul');
    tagListRootNode.setAttribute('class', 'key-column');

    const keyListUl = DataUpdater.renderFieldColumnContent(columnData);
    columnRootLiTag.appendChild(keyListUl);

    ulRoot.appendChild(columnRootLiTag);
  },

  renderFieldColumnContent: fieldKeyList => {
    const keyList = document.createElement('ul');
    keyList.setAttribute('data-method', 'renderFile');
    fieldKeyList.reduce(DataUpdater.keyListRenderer, keyList);
    return keyList;
  },

  keyListRenderer: (acc, jsonKey) => {
    const listItem = document.createElement('li');
    listItem.appendChild(document.createTextNode(jsonKey));
    acc.appendChild(listItem);
    return acc;
  },

  deduplicate: sortedArrayToDedupe => sortedArrayToDedupe.reduce((acc, jsonKey) => {
    if (acc[acc.length - 1] != jsonKey) acc.push(jsonKey);
    return acc;
  }, []),
}
