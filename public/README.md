# General comments to FE code

* The page makes repeated API requests to the domain it's running on and loads
  the file `public/example.api-data.json`. This file should be overwritten by
  the OpenShift process (*Yet to be done?*)
* The page was tested loading it under `localhosthost:8877`
* The frontend accepts a URL parameter of `interval` with a time in ms. By
  default, the timer is set to trigger once a second, but it can be overwritten
  with this URL parameter
* I "designed" the UI to look a little nicer, with breaking the fields into up
  to three columns. The UI also shows "dots" with "N" for non-public and
  "P" for public API endpoints.

# Running the code

The repo for the entire application (including my attempt to use `tcpflow`
is in [bitbucket](https://bitbucket.org/tmuller/tcpflow-pt/src/master/)).

To run the FrontEnd code, execute

```shell
node ./server.js
``` 

from the project's root directory. It is configured to use port 8877, so in your
browser, go to `http://localhost:8877`. The node application also contains a
backend, which is not being used. The HTML/JS/CSS are served from the `public`
folder in the project. That folder also contains the json file that should be
overwritten by the `oc` CL command, as `public/data-update.js` references
the stored file directly (in method `pollService()`, line 21).

# Outstanding tasks

1. Make sure that the file `public/example-api-data.json` is overwritten by the
   OpenShift CL statement


